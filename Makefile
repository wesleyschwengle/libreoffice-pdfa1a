all:
	zip -r pdfa1a.oxt -x=.git/* -x=.gitignore .

install:
	sudo unopkg add --shared pdfa1a.oxt --force

clean:
	rm pdfa1a.oxt

test:
	make all install clean
	libreoffice
